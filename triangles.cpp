/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References: http://www.cplusplus.com/articles/NhA0RXSz/ 
 * http://www.cplusplus.com/reference/algorithm/sort/
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 * Gerry: 2 hours
 */ 

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter()
{
	return s1+s2+s3;
}

unsigned long triangle::area()
{
	double area;
	int side1;
	int side2;
	if(s1 >= s2 && s1 >= s3)
	{
		side1 = s2;
		side2 = s3;
	}
	else if (s2 >= s1 && s2 >= s3)
	{
		side1 = s1;
		side2 = s3;
	}
	else
	{
		side1 = s1;
		side2 = s2;
	}
	area = (side1*side2)/2;
	return area;
}

void triangle::print()
{
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2)
{
	// TODO: write this function.
	int sideA = t1.s1;
	int sideB = t1.s2;
	int sideC = t1.s3;
	int sideD = t2.s1;
	int sideE = t2.s2;
	int sideF = t2.s3;

	unsigned long firstTriangle[3] = {sideA, sideB, sideC};
	sort(firstTriangle, firstTriangle + 3);

	unsigned long secondTriangle[3] = {sideD, sideE, sideF};
	sort(secondTriangle, secondTriangle + 3);

	if(firstTriangle[0] == secondTriangle[0] && firstTriangle[1] == secondTriangle[1] && firstTriangle[2] == secondTriangle[2])
	{
		return true;
	}
	else return false;
}

bool similar(triangle t1, triangle t2)
{
	// TODO: write this function.

	int sideA = t1.s1;
	int sideB = t1.s2;
	int sideC = t1.s3;
	int sideD = t2.s1;
	int sideE = t2.s2;
	int sideF = t2.s3;

	unsigned long triangle1[3]= {sideA,sideB,sideC};

	sort(triangle1, triangle1+3);

	unsigned long triangle2[3]= {sideD,sideE,sideF};

	sort(triangle2, triangle2+3);

	if(triangle1[0] * triangle2[1] == triangle2[0] * triangle1[1] && triangle1[1] * triangle2[2] == triangle2[1] * triangle1[2])
	{
		return true;
	}

	else return false;
}

	// TODO: find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h


vector<triangle> findRightTriangles(unsigned long l, unsigned long h)
{
	vector<triangle> retval;
	for(unsigned long perimeter = l; perimeter <= h; perimeter++)
	{
		for(unsigned long hyp = 1; hyp < perimeter; hyp++)
		{
			for(unsigned long mid = 1; mid < hyp; mid++)
			{
				for(unsigned long small = 1; small < mid; small++)
				{
					unsigned long hyps = hyp*hyp;
					unsigned long mids = mid*mid;
					unsigned long smalls = small*small;
							
					if(perimeter == hyp+mid+small && hyps == mids+smalls)
					{
						triangle t1(mid, small, hyp);
						retval.push_back(t1);								
					}
				}
			}
		}
	}
	return retval;
}
